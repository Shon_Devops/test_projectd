FROM python:3.6

COPY django /source/

RUN pip install --upgrade pip
RUN pip install django==2.0
RUN pip install uwsgi 
RUN pip install mysqlclient

#COPY . /app
#RUN pip install -r /app/requrements.txt # pip module install

WORKDIR /source
RUN chmod 755 /source/start.sh
EXPOSE 8000
CMD cd /source && ./start.sh
